/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * le kell reszetelni a gombokat az reset nél mert különben úgy marad a szinük 
 */
package hu.vodafone.bliss;

import com.codename1.components.ScaleImageButton;
import com.codename1.ui.Form;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Label;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.GridLayout;
import hu.vodafone.bliss.util.Config;
import hu.vodafone.bliss.util.word;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

/**
 *
 * @author RYZEN
 */
public class SzovegKitalalos extends Form {

    private ScaleImageButton[] buttons = new ScaleImageButton[4];
    private Label emblem;
    private Button reset;
    private Form current;
    private Resources theme;
    List<word> words = new ArrayList<word>();
    Hashtable<String, String> szavak = Config.getHangmanData();
    int truthbuttonnumber;

    SzovegKitalalos() {
        super("SzövegKitalálósdi");
    }

    @Override
    public void show() {
        super.show(); //To change body of generated methods, choose Tools | Templates.
        setupUI();
    }

    private void setupUI() {
        this.removeAll();
        theme = UIManager.initFirstTheme("/theme");
        Toolbar tb = this.getToolbar();
        tb = BlissToolBar.createToolbar(tb);
        this.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        Reset();
        Config.getSolution(words);
        createItem();
        setupVariable();
        setUpButton();
        this.add(emblem);
        Container cnt = new Container(new GridLayout(2, 2));
        for (Button button : buttons) {
            cnt.add(button);
        }
        this.add(cnt);
        this.add(reset);
        this.revalidate();
    }

    private void setUpButton() {
        for (int i = 0 ; i < 4 ; i++) {
            int j = i;
            buttons[j].getAllStyles().setBgTransparency(0xff);
            buttons[i].addActionListener(e -> {
                if (j==truthbuttonnumber){
                    buttons[j].getAllStyles().setBgColor(0x00ff00);
                    Config.disableButtons(buttons);
                }else{
                    buttons[j].setEnabled(false);
                    buttons[j].getAllStyles().setBgColor(0xff0000);
                }
            });
        }
    }
    
    private void setupVariable(){
        for (int i = 0 ; i < 4 ; i++){
            if(words.get(i).getTruth()==true){
                truthbuttonnumber = i;
                emblem.setText(words.get(i).getReply());
                Config.setButtonIcon(words.get(i).getImagePath(),buttons[i]);
            }else{
                Config.setButtonIcon(words.get(i).getImagePath(),buttons[i]);
            }
        }
    }

    private void Reset(){
        reset = new Button("Új feladvány");
        reset.addActionListener(e ->{
            Config.getSolution(words);
            setupVariable();
            Config.enableButtons(buttons);
            Config.styleButtons(buttons);
        });
    }

    private void createItem(){
        emblem = new Label();
        emblem.getAllStyles().setAlignment(CENTER);
        for (int i = 0 ; i < 4 ; i++) {
            buttons[i] = new ScaleImageButton();
        }
    }
}