/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.vodafone.bliss;

import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;

/**
 *
 * @author gabor
 */
public class BlissToolBar {

    public static Toolbar createToolbar(Toolbar tb) {
        Resources theme = UIManager.initFirstTheme("/theme");
        
        //tb.removeAll();

        Image icon = theme.getImage("bliss_red256.png");
        Image icon2 = theme.getImage("voda_logo_256.jpg");
        Container topBar = BorderLayout.north(new Label());
        //topBar = BorderLayout.south(new Label(icon));
        
        int fontSize = Display.getInstance().convertToPixels(6);
        Font ttfFont = Font.createTrueTypeFont("Vodafone", "Vodafone.ttf").
                      derive(fontSize, Font.STYLE_PLAIN);
        
        Label l = new Label("Bliss alkalmazás", "SidemenuTagline");
        l.getUnselectedStyle().setFont(ttfFont);
        l.getAllStyles().setFgColor(0xffffff);
        
        topBar.add(BorderLayout.WEST, icon);
        topBar.add(BorderLayout.EAST, icon2);
        topBar.add(BorderLayout.SOUTH, l);
        topBar.setUIID("SideCommand");
        tb.addComponentToSideMenu(topBar);

        tb.addMaterialCommandToSideMenu("Bliss Alapítványról", FontImage.MATERIAL_HOME, e -> {
            Form about = new Home();
            about.show();
        });
        tb.addMaterialCommandToSideMenu("Érintett vagyok-e", FontImage.MATERIAL_WEB, e -> {
            Form erintett = new Erintettvagyok();
            erintett.show();
        });
         tb.addMaterialCommandToSideMenu("Eszközkölcsönző", FontImage.MATERIAL_WEB, e -> {
//            Form rent = new RentHome();
//            rent.show();
                Form kolcsmenete = new KolcsonzesMenete();
                kolcsmenete.show();
        });
        tb.addMaterialCommandToSideMenu("Szakemberkereső", FontImage.MATERIAL_WEB, e -> {
            Form specialist = new SpecialistSearch();
            specialist.show();
        });
        tb.addMaterialCommandToSideMenu("Játékok épeknek", FontImage.MATERIAL_SETTINGS, e -> {
            Form hangman = new GameHomeEp();
            hangman.show();
        });
         tb.addMaterialCommandToSideMenu("Játékok sérülteknek", FontImage.MATERIAL_SETTINGS, e -> {
            Form hangman = new GameHomeSerult();
            hangman.show();
        });
        tb.addMaterialCommandToSideMenu("Projektről", FontImage.MATERIAL_INFO, e -> {
            Form projektrol = new Projektrol();
            projektrol.show();
        });


        return tb;
    }

}
