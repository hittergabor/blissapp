package hu.vodafone.bliss;

import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;

/**
 *
 * @author gabor
 */
public class RentHome extends Form {

    public static byte row = 4;
    public static byte column = 2;

    public static byte getRow() {
        return row;
    }

    public static void setRow(byte row) {
        Game.row = row;
    }

    public static byte getColumn() {
        return column;
    }

    public static void setColumn(byte column) {
        Game.column = column;
    }

    private Resources theme;

    RentHome() {
        super("Kölcsönzés");
    }

    @Override
    public void show() {
        super.show(); //To change body of generated methods, choose Tools | Templates.
        setupUI();
    }

    private void setupUI() {
        this.removeAll();
        theme = UIManager.initFirstTheme("/theme");

        Toolbar tb = this.getToolbar();
        tb = BlissToolBar.createToolbar(tb);

        this.setLayout(new BorderLayout());

        Button small = new Button("Kölcsönzés menete");
        small.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                setRow((byte) 4);
                setColumn((byte) 2);
                Form kolcsmenete = new KolcsonzesMenete();
                kolcsmenete.show();
            }
        });
        Button medium = new Button("Eszközök");
        medium.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                setRow((byte) 4);
                setColumn((byte) 4);
                Form memoria = new Rent();
                memoria.show();
            }
        });
        
        Label label = new Label("Memória játékok");
        label.getAllStyles().setAlignment(CENTER);
        Container cnt = new Container(new GridLayout(3, 1));
        cnt.add(small);
        //cnt.add(medium);
        this.add(BorderLayout.CENTER, cnt);
        this.revalidate();
    }

}
