package hu.vodafone.bliss;

import com.codename1.ui.BrowserComponent;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;

import com.codename1.ui.Button;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.table.DefaultTableModel;
import com.codename1.ui.table.Table;
import com.codename1.ui.table.TableLayout;
import com.codename1.ui.table.TableModel;

import com.codename1.ui.Component;

/**
 *
 * @author gabor
 */
public class SpecialistSearch extends Form {

    private Container cnt;
    private Resources theme;
    BrowserComponent browser = new BrowserComponent();
    public static int selectedrow = 0;

    SpecialistSearch() {
        super("Szakemberkereső");
    }

    @Override
    public void show() {
        super.show(); //To change body of generated methods, choose Tools | Templates.
        setupUI();
    }

    public static int getSelectedrow() {
        return selectedrow;
    }

    public static void setSelectedrow(int selectedrow) {
        SpecialistSearch.selectedrow = selectedrow;
    }
    
    

    private void setupUI() {
        this.removeAll();
        theme = UIManager.initFirstTheme("/theme");

        Toolbar tb = this.getToolbar();
        tb = BlissToolBar.createToolbar(tb);

        this.setFormBottomPaddingEditingMode(true);

        this.setLayout(new BorderLayout());

        TableModel model = new DefaultTableModel(new String[]{"Név", "Város", "Email"}, new Object[][]{
            {"Farkas-Villányi Dóra", "Budapest, 12. kerület", "villdori@gmail.com"},
            {"Fekete-Szabó Viola", "Budapest 11. kerület", "fszvivi@gmail.com"},
            {"Földesiné Irmalós Erzsébet", "Tóalmás", "irmaloserzsebet@gmail.com"},
            {"Vargáné Nagy Anna", "Budapest 22. kerület", "holdvilag.info@gmail.com"},
            {"Jékey Franciska", "Budapest 7. kerület", "kitlike2@gmail.com"},
            {"Farkas-László Tímea ", "Dunakeszi", "tomka2003@gmail.com "},
            {"Marofka Istvánné", "Budapest 16. kerület", "margom62@gmail.com"},
            {"Verbó Szemőke", "Pilisvörösvár", "sz.verbo@gmail.com"},
            {"Tardi Anita", "Budapest 17. kerület", "tardianita79@freemail.hu"},
            {"Kenézné Ádám Zsuzsannna", "Tiszafüred", "kenezzsu@gmail.com"},
            {"Gubányi Zsófia ", "Kosd", ""},
            {"Tóthné Horváth Patrícia", "Budapest 23. kerület", ""},}) {
        };
        Table table = new Table(model) {
            @Override
            protected Component createCell(Object value, int row, int column, boolean editable) { // (1)
                Component cell;

                if (row < 0) {
                    cell = super.createCell(value, row, column, editable);
                } else {
                    cell = new Button(value.toString());
                    cell.setUIID("TableCell");
                    ((Button) cell).addActionListener(e -> {
                        setSelectedrow(row+1);
//                        Form specialistMap = new SpecialistSearchMap();
//                        specialistMap.show();
                    });
                }
                if (row > -1 && row % 2 == 0) { // (5)
                    // pinstripe effect 
                    cell.getAllStyles().setBgColor(0xeeeeee);
                    cell.getAllStyles().setBgTransparency(255);
                }

                return cell;
            }

            @Override
            protected TableLayout.Constraint createCellConstraint(Object value, int row, int column) {
                TableLayout.Constraint con = super.createCellConstraint(value, row, column);
                if (row == 1 && column == 1) {
                    con.setHorizontalSpan(2);
                }
                //con.setWidthPercentage(33);
                return con;
            }
        };
        table.setScrollableX(true);

        this.add(BoxLayout.X_AXIS, table);
        this.revalidate();

    }

}
