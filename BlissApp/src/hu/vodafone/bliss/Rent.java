package hu.vodafone.bliss;

/**
 * én SpanLabel helyett textarea -t taléltam de ez lehet hogy jobb, gondoltam én
 * tudatlan egymás mellé tehetnénk a két gombot de nem tudtam át méretezni :( én
 * containert használtam nem biztos hogy kell
 */
import com.codename1.components.InfiniteProgress;
import com.codename1.components.SpanLabel;
import com.codename1.components.WebBrowser;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.BrowserComponent;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.CENTER;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.RoundBorder;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Iterator;

/**
 *
 * @author gabor
 */
public class Rent extends Form {

    private Container cnt;
    private Resources theme;
    BrowserComponent browser = new BrowserComponent();

    Rent() {
        super("Eszközkölcsönzés");
    }

    @Override
    public void show() {
        super.show(); //To change body of generated methods, choose Tools | Templates.
        setupUI();
        getRents();
    }

    private void setupUI() {
        this.removeAll();
        theme = UIManager.initFirstTheme("/theme");

        Toolbar tb = this.getToolbar();
        tb = BlissToolBar.createToolbar(tb);

        this.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        this.revalidate();
    }

    protected void getRents() {

        ConnectionRequest r = new ConnectionRequest() {
            @Override
            protected void postResponse() {

            }

            @Override
            protected void readResponse(InputStream input) throws IOException {
                JSONParser p = new JSONParser();
                Hashtable h = p.parse(new InputStreamReader(input));
                Vector data = (Vector) h.get("data");
                if (!data.isEmpty()) {
                    Iterator value = data.iterator();
                    while (value.hasNext()) {
                        Hashtable item = (Hashtable) value.next();
                        String id = (String) item.get("id");
                        String date = (String) item.get("date");
                        String text = (String) item.get("text");
                        String label = (String) item.get("label");
                        setContainerforInfo();
                        setItems(label, text, date, id);
                        addItem(cnt);
                    }
                }

                String htmlTxt = "";
//                        (String)h.get("data");
                System.out.println("" + htmlTxt);
                browser.setPage(htmlTxt, null);
            }
        };
        r.setUrl("http://www.hade.webtelek.hu/api");
        r.setPost(false);
        r.addArgument("type", "rent");

        InfiniteProgress prog = new InfiniteProgress();
        Dialog dlg = prog.showInfiniteBlocking();
        r.setDisposeOnCompletion(dlg);
        NetworkManager.getInstance().addToQueueAndWait(r);
        r.getResponseData();

    }

    //add Containers to Form --> (this)
    private void addItem(Container item) {
        this.add(item);
    }

    //Create Browser and Browser Dialog for more Info
    private void addBrowserDialog(String htmltext) {
        WebBrowser browser = new WebBrowser("");
        Dialog webDialog = new Dialog("Részletek");
        webDialog.setLayout(new BorderLayout());
        browser.setPage(htmltext, null);
        webDialog.addComponent(BorderLayout.EAST, browser);
        Button ok = new Button("Kilépés");
        ok.addActionListener(e -> {
            webDialog.dispose();
        });
        webDialog.add(BorderLayout.SOUTH, ok);
        webDialog.show();
    }

    //Create Container for Items with data
    private void setContainerforInfo() {
        cnt = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        cnt.getAllStyles().setPadding(10, 10, 10, 10);
        cnt.getAllStyles().setMargin(20, 20, 20, 20);
        cnt.getAllStyles().setBgTransparency(255);
        cnt.getStyle().setBorder(Border.createLineBorder(1, 0xff0000));
        //cnt.getAllStyles().setBgColor(0xff0000);
    }

    //Set Item (Button,TextArea) for texts and add ActionListener for WebButton and RentButton
    private void setItems(String label, String text, String date, String id) {
        Label title = new Label(label);
        SpanLabel centerText = new SpanLabel(text);
        Label endDate = new Label("A Kölcsönzés lejár: " + date);
        Button rent = new Button("Kikölcsönzöm");
        Button cancel = new Button("Visszaadom");
        Button webView = new Button("Részletek");
        cnt.add(title);
        //Title center alignment
        title.getAllStyles().setAlignment(CENTER);
        cnt.add(centerText);
        addButtonLeft(webView);
        if (date.equals("0000-00-00")) {
            //When Have not Rent-End date addButton for Renting
            rent.setVisible(true);
            cancel.setVisible(false);   
            addButtonRight(rent);
            System.out.println("A következő elem szabad: " + id);
        } else {
            //Or add Only the Ending date
            rent.setVisible(false);
            cancel.setVisible(true);
            addLabelRight(endDate);
            addButtonCancel(cancel);
        }
        //AddAction for Buttons
        addActListRentButton(rent, id);
        addActListCancelButton(cancel, id);
        addActListWebButton(webView, text);
    }

    //Create Renting Button Action
    private void addActListRentButton(Button RentButton, String id) {
        RentButton.addActionListener(e -> {
            //Dialog.show("Error", "You are in Wrong Place" , "Close", null);
            Form rentWindow = new RentWindow(id);
            rentWindow.show();
        });
    }

    private void addActListCancelButton(Button RentButton, String id) {
        RentButton.addActionListener(e -> {
            BrowserComponent browser = new BrowserComponent();
             browser.setURL("http://admin.blissalapitvany.hu/api?type=cancel&id="+id);
             Dialog dg= new Dialog();
             dg.show("Értesítés", "Sikeres lemondás!", "OK", "");
            Form rent = new Rent();
            rent.show();
        });
    }
    
    //Create WebView Button Action
    private void addActListWebButton(Button WebView, String htmlText) {
        WebView.addActionListener(e -> {
            addBrowserDialog(htmlText);
        });
    }

    private void addButtonRight(Button item) {
        item.setAlignment(cnt.CENTER); // Tudom, elavult. De ez az egyetlen mód, ami működik is. --> kösz :)
        cnt.add(item);
    }
    
    private void addButtonCancel(Button item) {
        item.setAlignment(cnt.CENTER); // Tudom, elavult. De ez az egyetlen mód, ami működik is. --> kösz :)
        cnt.add(item);
    }

    private void addLabelRight(Label item) {
        item.setAlignment(cnt.CENTER); // Tudom, elavult. De ez az egyetlen mód, ami működik is. --> thanks :)
        cnt.add(item);
    }

    private void addButtonLeft(Button item) {
        item.setAlignment(cnt.CENTER); // Tudom, elavult. De ez az egyetlen mód, ami működik is. --> köszike :)
        item.setWidth(100);//gondoltam egymás mellé tehetjük a gombokat de nekem nem sikerült átméretezni :(
        cnt.add(item);
    }
    //én containert használtam nem biztos hogy kell
//    private void addLongItem(SpanLabel item){
//        this.add(item); 
//    }
}
