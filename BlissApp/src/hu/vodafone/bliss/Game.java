package hu.vodafone.bliss;

import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;

/**
 *
 * @author gabor
 */
public class Game extends Form {

    public static byte row = 4;
    public static byte column = 2;

    public static byte getRow() {
        return row;
    }

    public static void setRow(byte row) {
        Game.row = row;
    }

    public static byte getColumn() {
        return column;
    }

    public static void setColumn(byte column) {
        Game.column = column;
    }

    private Resources theme;

    Game() {
        super("Memóriajáték");
    }

    @Override
    public void show() {
        super.show(); //To change body of generated methods, choose Tools | Templates.
        setupUI();
    }

    private void setupUI() {
        this.removeAll();
        theme = UIManager.initFirstTheme("/theme");

        Toolbar tb = this.getToolbar();
        tb = BlissToolBar.createToolbar(tb);

        this.setLayout(new BorderLayout());

        Button small = new Button("4x2");
        small.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                setRow((byte) 4);
                setColumn((byte) 2);
                Form memoria = new Memoria();
                memoria.show();
            }
        });
        Button medium = new Button("4x4");
        medium.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                setRow((byte) 4);
                setColumn((byte) 4);
                Form memoria = new Memoria();
                memoria.show();
            }
        });
        Button large = new Button("6x4");
        large.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                setRow((byte) 6);
                setColumn((byte) 4);
                Form memoria = new Memoria();
                memoria.show();
            }
        });
        Label label = new Label("Memória játékok");
        label.getAllStyles().setAlignment(CENTER);
        Container cnt = new Container(new GridLayout(3, 1));
        cnt.add(small);
        cnt.add(medium);
        cnt.add(large);
        this.add(BorderLayout.CENTER, cnt);
        this.revalidate();
    }

}
