/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.vodafone.bliss;

import com.codename1.ui.Display;
import com.codename1.ui.Font;
import com.codename1.ui.Label;

/**
 *
 * @author mac
 */
public class Label2 extends Label {
    
    Label2(String text, int fontsize) {
        super(text);
        
        int fontSize = Display.getInstance().convertToPixels(fontsize);
        Font ttfFont = Font.createTrueTypeFont("Vodafone", "Vodafone.ttf").
                      derive(fontSize, Font.STYLE_PLAIN);
        this.getUnselectedStyle().setFont(ttfFont);
        this.getAllStyles().setFgColor(0xffffff);
    }
  
}
