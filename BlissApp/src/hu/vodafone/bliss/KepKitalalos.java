/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.vodafone.bliss;

import com.codename1.components.ScaleImageLabel;
import com.codename1.ui.Form;
import com.codename1.ui.Button;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BoxLayout;
import hu.vodafone.bliss.util.Config;
import hu.vodafone.bliss.util.word;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

/**
 *
 * Még nincs teljesen kész pl még nincs reszet és arra gondoltam hogy nem dialog lesz hanem elszineződnek a gombok
 * a helyes megoldásra kattintva zöld lesz a gomb a helytelenre piros és a helyes kizöldül (persze csak ha meg tudom csinálni :))
 * gombok alapszine egyébként fehér lesz vagy világos kék
 * 
 */
public class KepKitalalos extends Form {

    private Button[] buttons = new Button[4];
    private ScaleImageLabel emblem;
    private Button reset;
    private Form current;
    private Resources theme;
    List<word> words = new ArrayList<word>();
    Hashtable<String, String> szavak = Config.getHangmanData();
    int truthbuttonnumber;

    KepKitalalos() {
        super("Képkitalálósdi");
    }

    @Override
    public void show() {
        super.show(); //To change body of generated methods, choose Tools | Templates.
        setupUI();
    }

    private void setupUI() {
        this.removeAll();
        theme = UIManager.initFirstTheme("/theme");
        Toolbar tb = this.getToolbar();
        tb = BlissToolBar.createToolbar(tb);
        this.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        Reset();
        Config.getSolution(words);
        createItem();
        setupVariable();
        setUpButton();
        this.add(emblem);
        for (Button button : buttons) {
            this.add(button);
        }
        this.add(reset);
        this.revalidate();
    }

    private void setUpButton() {
        for (int i = 0 ; i < 4 ; i++) {
            int j = i;
            buttons[j].getAllStyles().setBgTransparency(0xff);
            buttons[i].addActionListener(e -> {
                if (j==truthbuttonnumber){
                    buttons[j].getAllStyles().setBgColor(0x00ff00);
                    Config.disableButtons(buttons);
                }else{
                    buttons[j].setEnabled(false);
                    buttons[j].getAllStyles().setBgColor(0xff0000);
                }
            });
        }
    }

    private void setupVariable(){
        for (int i = 0 ; i < 4 ; i++){
            buttons[i].getAllStyles().setBgTransparency(0xff);
            buttons[i].getAllStyles().setBgColor(0xffffff,true);
            if(words.get(i).getTruth()==true){
                truthbuttonnumber = i;
                Config.setLabelIcon(words.get(i).getImagePath(), emblem);
                buttons[i].setText(words.get(i).getReply());
            }else{
                buttons[i].setText(words.get(i).getReply());
            }
        }
    }

    private void Reset(){
        reset = new Button("Új feladvány");
        reset.addActionListener(e ->{
            Config.getSolution(words);
            setupVariable();
            Config.enableButtons(buttons);
        });
    }

    private void createItem(){
        emblem = new ScaleImageLabel();
        for (int i = 0 ; i < 4 ; i++) {
            buttons[i] = new Button();
            buttons[i].setUIID("MultiButton");
        }
    }
}