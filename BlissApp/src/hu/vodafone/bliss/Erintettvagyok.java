package hu.vodafone.bliss;

import com.codename1.components.InfiniteProgress;
import com.codename1.components.WebBrowser;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Hashtable;

/**
 *
 * @author gabor
 */
public class Erintettvagyok extends Form {

    private Resources theme;
    WebBrowser browser = new WebBrowser("");

    Erintettvagyok() {
        super("Mikor érdemes vizsgálatra jelentkezni / jönni a Bliss Alapítványhoz");
    }
    @Override
    public void show() {
        super.show(); //To change body of generated methods, choose Tools | Templates.
        setupUI();
    }
    
    private void setupUI() {
        this.removeAll();
        theme = UIManager.initFirstTheme("/theme");

        Toolbar tb = this.getToolbar();
        tb = BlissToolBar.createToolbar(tb);

        this.setLayout(new BorderLayout());
                
        WebBrowser browser = new WebBrowser(""); 
        browser.setURL("https://blissalapitvany.hu/erintett-vagyok-e/");
        this.addComponent(BorderLayout.CENTER, browser); 
        
        this.revalidate();
    }
}
