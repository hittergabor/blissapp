package hu.vodafone.bliss;

import com.codename1.maps.Coord;
import com.codename1.ui.BrowserComponent;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;

import com.codename1.googlemaps.MapContainer;
import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.FontImage;

/**
 *
 * @author gabor
 */
public class SpecialistSearchMap extends Form {

    private Container cnt;
    private Resources theme;
    BrowserComponent browser = new BrowserComponent();

    SpecialistSearchMap() {
        super("Szakemberkereső");
    }

    @Override
    public void show() {
        super.show(); //To change body of generated methods, choose Tools | Templates.
        System.out.println("selected row: "+SpecialistSearch.getSelectedrow());
        setupUI();
    }

    private void setupUI() {
        this.removeAll();
        theme = UIManager.initFirstTheme("/theme");

        Toolbar tb = this.getToolbar();
        tb = BlissToolBar.createToolbar(tb);

        this.setFormBottomPaddingEditingMode(true);

        this.setLayout(new BoxLayout(BoxLayout.Y_AXIS));

        MapContainer mc = new MapContainer();
        
        double x = 47.497912;
        double y = 19.040235;
        
        if (SpecialistSearch.getSelectedrow() == 0) {
             x = 47.497912;
             y = 19.040235;
        }
        
        Coord coord = new Coord(x, y);
        
        // Teszt marker, gombbal - Budapest
        Button mapButton = new Button("");
        FontImage.setMaterialIcon(mapButton, FontImage.MATERIAL_PLACE);
        mc.addMarker(mapButton, coord);
               
        mc.zoom(coord, mc.getMaxZoom() - 2);
        mc.setCameraPosition(coord);        
        this.addComponent(mc);
        addActionInfoButton(mapButton);

    }
        private void addActionInfoButton(Button InfoButton){
             InfoButton.addActionListener(e -> {
             Dialog dg= new Dialog();
             dg.show("Információk:" + SpecialistSearch.getSelectedrow() , "Név: \n E-mail cím: \n Cím: \n", "OK", "");
            
        });

    }
            

}
