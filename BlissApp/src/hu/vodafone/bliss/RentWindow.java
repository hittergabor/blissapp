package hu.vodafone.bliss;

import com.codename1.components.InfiniteProgress;
import com.codename1.components.WebBrowser;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.DateFormat;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.BrowserComponent;
import com.codename1.ui.Button;
import com.codename1.ui.Calendar;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.PickerComponent;
import com.codename1.ui.TextComponent;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import static com.codename1.ui.events.ActionEvent.Type.Calendar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import hu.vodafone.bliss.BlissToolBar;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Hashtable;

/**
 *
 * @author ljozsefmark
 */
public class RentWindow extends Form {

    private Resources theme;
    WebBrowser browser = new WebBrowser(""); 
    private Container cont;
    private String datumertek;
    private String emailertek;
    private TextComponent email;
    private PickerComponent date;
    private Form Back = new Rent();
    private String id;
    

    RentWindow() {
        super("Kikölcsönzöm");
    }
    
    RentWindow(String id) {
        super("Kikölcsönzöm");
        this.id = id;
    }

    @Override
    public void show() {
        super.show(); //To change body of generated methods, choose Tools | Templates.
        setupUI();
    }

    private void setupUI() {
        this.removeAll();
        theme = UIManager.initFirstTheme("/theme");
        
        Toolbar tb = this.getToolbar();
        tb = BlissToolBar.createToolbar(tb);
        
        this.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        
        email = new TextComponent().label("E-Mail cím:");
        date = PickerComponent.createDate(new Date()).label("Dátum"); // Még egy dátumos, ez kicsit egyszerűbb! 
        Button reserve = new Button("Lefoglal");
        Button cancel= new Button("Mégse");
        this.add(email);
        this.add(date);
        this.add(reserve);
        this.add(cancel);
        System.out.println(datumertek);
        addActListResButton(reserve);
        addCancelButtonAction(cancel, Back);
        
        this.revalidate();
    }

        private void addActListResButton(Button ReserveButton){
             ReserveButton.addActionListener(e -> {
             email.getText();
             String emailertek=email.getText();            
             Date datumertek = (Date)date.getPicker().getDate();
             DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
             String DateToString=dateFormat.format(datumertek);
             System.out.println("email: "+emailertek);
             System.out.println("dátum: "+datumertek);
             System.out.println("dátum: "+DateToString);
             BrowserComponent browser = new BrowserComponent();
             browser.setURL("http://admin.blissalapitvany.hu/api?type=book&id="+id+"&email="+emailertek+"&todate="+DateToString+"");
             Dialog dg= new Dialog();
             dg.show("Értesítés", "Sikeres foglalás!", "OK", "");
             Back.show();
            
        });

    }
                
        private void addCancelButtonAction(Button cancelButton, Form Destination){
             cancelButton.addActionListener(e -> {
                 Destination.show();
        });

    }
}
