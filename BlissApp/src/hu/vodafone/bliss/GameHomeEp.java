package hu.vodafone.bliss;

import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;

/**
 *
 * @author gabor
 */
public class GameHomeEp extends Form {

    public static byte row = 4;
    public static byte column = 2;

    public static byte getRow() {
        return row;
    }

    public static void setRow(byte row) {
        GameHomeEp.row = row;
    }

    public static byte getColumn() {
        return column;
    }

    public static void setColumn(byte column) {
        GameHomeEp.column = column;
    }

    private Resources theme;

    GameHomeEp() {
        super("Játékok");
    }

    @Override
    public void show() {
        super.show(); //To change body of generated methods, choose Tools | Templates.
        setupUI();
    }

    private void setupUI() {
        this.removeAll();
        theme = UIManager.initFirstTheme("/theme");

        Toolbar tb = this.getToolbar();
        tb = BlissToolBar.createToolbar(tb);

        this.setLayout(new BorderLayout());

        Button small = new Button("Akasztófa");
        small.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                setRow((byte) 4);
                setColumn((byte) 2);
                Form memoria = new Hangman();
                memoria.show();
            }
        });
        Button medium = new Button("Memória");
        medium.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                setRow((byte) 4);
                setColumn((byte) 4);
                Form memoria = new Game();
                memoria.show();
            }
        });
        Button medium2 = new Button("Képkitaláló");
        medium2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                setRow((byte) 4);
                setColumn((byte) 4);
                Form memoria = new KepKitalalos();
                memoria.show();
            }
        });
        Button medium3 = new Button("Szövegkitaláló");
        medium3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                setRow((byte) 4);
                setColumn((byte) 4);
                Form memoria = new SzovegKitalalos();
                memoria.show();
            }
        });
        Label label = new Label("Játékok épeknek");
        label.getAllStyles().setAlignment(CENTER);
        Container cnt = new Container(new GridLayout(3, 1));
        cnt.add(small);
        cnt.add(medium);
        cnt.add(medium2);
        cnt.add(medium3);
        this.add(BorderLayout.CENTER, cnt);
        this.revalidate();
    }

}
