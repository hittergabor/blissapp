package hu.vodafone.bliss;
/*
* ScaleImageButton sajnos csak akkor használható ha nincs rajta szöveg
* ScaleImageButton a képek igazítására lett volna használható
* https://www.codenameone.com/javadoc/com/codename1/components/ScaleImageButton.html#setText-java.lang.String-
* import com.codename1.components.ScaleImageButton;
*/

import com.codename1.components.ScaleImageButton;
import com.codename1.components.SpanLabel;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Toolbar;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.ui.animations.FlipTransition;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Style;
import hu.vodafone.bliss.util.Config;
import java.util.Hashtable;

public class Memoria extends Form {

    Hashtable<String, String> szavak = Config.getHangmanData();
    private Resources theme;
    static private byte row;
    static private byte column;
    static private ScaleImageButton[][] buttons;
    private ArrayList<String> keys = new ArrayList<>();
    private ArrayList<Integer> twoitem = new ArrayList<>();
    private Container cnt;
    private int upset = 0;
    private int test = 0;

    Memoria() {
        super("Memória játék");
    }

    @Override
    public void show() {
        super.show(); //To change body of generated methods, choose Tools | Templates.
        setupUI();
    }

    private void setupUI() {
        this.removeAll();
        theme = UIManager.initFirstTheme("/theme");

        Toolbar tb = this.getToolbar();
        tb = BlissToolBar.createToolbar(tb);
        this.setLayout(new BorderLayout());
        Style s = UIManager.getInstance().getComponentStyle("Label");
        s.setFgColor(0xff0000);
        s.setBgTransparency(0);
        get();
        cnt = new Container(new GridLayout(row, column));
        createbutton();
        for (int i = 0; i < buttons[0].length; i++) {
            buttonupset(i);
        }
        this.add(BorderLayout.CENTER, cnt);
        this.revalidate();
    }

    public void createbutton() {
        for (int i = 0; i < buttons[0].length; i++) {
            buttons[0][i] = new ScaleImageButton();
            buttons[0][i].setUIID("Button2");
            cnt.add(buttons[0][i]);
        }
        shuffleArray(keys);
        for (int i = 0; i < buttons[0].length; i++) {
            buttons[1][i] = new ScaleImageButton();
            buttons[1][i].setTextPosition(Component.BOTTOM);
            buttons[1][i].setEnabled(false);
            Config.setButtonIcon("/" + szavak.get(keys.get(i)),buttons[1][i]);
        }
    }

    public void buttonupset(int i) {
        buttons[0][i].addActionListener(e -> {
            if (upset < 2) {
                Config.disableButtons(buttons[0]);
                buttons[0][i].getParent().replaceAndWait(buttons[0][i], buttons[1][i], new FlipTransition(0xffffff, 500));
                twoitem.add(i);
                Config.enableButtons(buttons[0]);
                upset++;
                if (upset == 2) {
                    Config.disableButtons(buttons[0]);
                    if (!keys.get(twoitem.get(1)).equals(keys.get(twoitem.get(0)))) {
                        buttons[1][twoitem.get(0)].getParent().replaceAndWait(buttons[1][twoitem.get(0)],
                                buttons[0][twoitem.get(0)], new FlipTransition(0xffffff, 500));
                        buttons[1][twoitem.get(1)].getParent().replaceAndWait(buttons[1][twoitem.get(1)],
                                buttons[0][twoitem.get(1)], new FlipTransition(0xffffff, 500));
                        twoitem.clear();
                        upset = 0;
                        Config.enableButtons(buttons[0]);
                    } else {
                        test();
                        twoitem.clear();
                        upset = 0;
                        Config.enableButtons(buttons[0]);
                    }
                }
            }
        });
    }
    // https://stackoverflow.com/questions/1519736/random-shuffling-of-an-array

    static void shuffleArray(ArrayList ar) {
        Random rnd = new Random();
        for (int i = ar.size() - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            String a = (String) ar.get(index);
            ar.set(index, ar.get(i));
            ar.set(i, a);
        }
    }

    public void test() {
        test++;
        if (test == keys.size() / 2) {
            //https://www.codenameone.com/javadoc/com/codename1/ui/Dialog.html
            Dialog d = new Dialog("Nyertél");
            d.add(new SpanLabel("Ügyes Voltás"));
            Button ok = new Button("OK");
            Button back = new Button("Új játék");
            Button home = new Button("Home");
            d.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
            ok.addActionListener(e -> {
                d.dispose();//dialog bezárása
            });
            back.addActionListener(e -> {
                Form memoria = new Game();
                memoria.show();
            });
            home.addActionListener(e -> {
                Form about = new Home();
                about.show();
            });
            d.add(ok);
            d.add(back);
            d.add(home);
            d.show();
        }
    }

    private void get() {
        Memoria.row = Game.getRow();
        Memoria.column = Game.getColumn();
        buttons = new ScaleImageButton[2][row * column];
        keys.clear();
        int j = 0;
        while (j < row * column / 2) {
            String sv = Config.getRandomText(szavak);
            if (!keys.contains(sv)) {
                keys.add(sv);
                keys.add(sv);
                j++;
            }
        }
    }
}
//        szo = Config.getRandomText(szavak);