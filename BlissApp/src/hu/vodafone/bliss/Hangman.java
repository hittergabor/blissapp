package hu.vodafone.bliss;

import com.codename1.components.ScaleImageLabel;
import com.codename1.ui.Form;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Label;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.GridLayout;
import java.util.ArrayList;

import com.codename1.ui.layouts.FlowLayout;
import hu.vodafone.bliss.util.Config;
import com.codename1.ui.Image;
import com.codename1.ui.layouts.GridLayout;

import java.util.Hashtable;
import java.util.Random;

/**
 *
 * @author gabor
 */
public class Hangman extends Form {

    String letters = "öüóqwertzuiopőúasdfghjkléáűíyxcvbnm";
    private Button[] buttons = new Button[35];
    private Button reset;
    Label lab, lab1, elet1;
    ScaleImageLabel photo;
    Container cnt = null;
    private Form current;
    private Resources theme;
    private byte elet = 11;
    private byte van = 0;
    String szo = "macska";
    String kep = "cat.png";
    ArrayList<Character> hasznalt = new ArrayList<>();
    ArrayList<Character> kitalalt = new ArrayList<>();
    ArrayList<Character> alap = new ArrayList<>();
    
    Hashtable<String, String> szavak = Config.getHangmanData();

    Hangman() {
        super("Játék");
    }

    @Override
    public void show() {
        super.show(); //To change body of generated methods, choose Tools | Templates.
        setupUI();
    }

    private void setupUI() {
        this.removeAll();
        photo = new ScaleImageLabel(); //photo = new Label(); 
        
        theme = UIManager.initFirstTheme("/theme");
        Toolbar tb = this.getToolbar();
        tb = BlissToolBar.createToolbar(tb);

        this.setLayout(new BoxLayout(BoxLayout.Y_AXIS));
        
        elet1 = new Label("Élet: " + elet);
        elet1.getAllStyles().setAlignment(CENTER);
        lab = new Label();
        lab.getAllStyles().setBgTransparency(255);
        lab.getAllStyles().setBgColor(0xff0000);
        lab.getAllStyles().setAlignment(CENTER);
        lab1 = new Label("Akasztófa");
        lab1.getAllStyles().setBgTransparency(255);
        lab1.getAllStyles().setBgColor(0xff0000);
        lab1.getAllStyles().setAlignment(CENTER);
        
        // kép középre
        Container center = FlowLayout.encloseCenterMiddle();
        center.add(photo);
        
        
        this.addComponent(center);
        this.add(elet1);
        this.add(lab);
        this.add(lab1);
        cnt = new Container(new GridLayout(5,7));
        this.add(cnt);
        createButton();
        reset = new Button("Új szó");
        resetbutton();
        reset();
        this.add(reset);

        this.revalidate();
    }

    public void resetbutton() {
        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        });
    }

    public void createButton() {
        // MyApplication M = new MyApplication();
        for (int i = 0; i < buttons.length; i++) {
            String sv = String.valueOf(letters.charAt(i));
            buttons[i] = new Button(sv);
            cnt.add(buttons[i]);
            button(buttons[i]);
        }
    }

    public void button(Button name) {
        name.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                name.setEnabled(false);
                vizsgalat(name.getText().toLowerCase().charAt(0));
            }
        });
    }

    public void vizsgalat(char tipp) {
        van = 0;
        for (int j = 0; j < alap.size(); j++) {
            if (tipp == alap.get(j)) {
                kitalalt.set(j, tipp);
                lab.setText(kiir(kitalalt));
                ++van;

            } else if (j == (alap.size() - 1) && van == 0) {
                lab1.setText("Nem talált!");
                lab.setText(kiir(kitalalt));
                elet--;
                elet1.setText("Élet: " + elet);
            }

        }
        if (elet == 0) {
            lab1.setText("Nem sikerült kitalálnod!");
            lab1.setText("A megfejtés: " + szo);
            buttonStop();
            van = 0;
        }
        if (!kitalalt.contains('-')) {
            lab1.setText("Kitaláltad!");
            buttonStop();
        }

    }
    
    public void randomStart() {
        szo = Config.getRandomText(szavak);
        System.out.println(szavak.get(szo));
        kep = szavak.get(szo);
        //szavak.remove(szo);                                                   //gondolta ki kommentezem mert addig hibát dob mert nincs sok képünk de így jöhet egymás után
        feltolt(szo);
        lab.setText(kiir(kitalalt));
        Image img;
            try {
                img = Image.createImage("/"+kep);
                photo.setIcon(img);
                
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public void feltolt(String szo) {
        alap.clear();
        kitalalt.clear();
        for (int i = 0; i < szo.length(); i++) {
            alap.add(szo.charAt(i));
            kitalalt.add('-');
        }
    }

    public String kiir(ArrayList<Character> tomb) {
        String sv = "";
        for (int j = 0; j < tomb.size(); j++) {
            sv = sv.concat(String.valueOf(tomb.get(j))).concat(" ");
        }
        return sv;
    }

    public void buttonStop() {
        for (int i = 0; i < buttons.length; i++) {
            buttons[i].setEnabled(false);
        }
    }

    public void reset() {
        for (int i = 0; i < buttons.length; i++) {
            buttons[i].setEnabled(true);
        }
        lab1.setText("Új szó");
        elet = 11;
        elet1.setText("Élet: " + elet);
        randomStart();
    }
}