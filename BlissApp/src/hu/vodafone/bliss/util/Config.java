/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.vodafone.bliss.util;

import com.codename1.ui.Button;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

public class Config {

    public static Hashtable<String, String> getHangmanData() {
        Hashtable<String, String> text = new Hashtable();
        text.put("macska", "cat.png");
        text.put("atom", "atom.png");
        text.put("korona", "crown.png");
        text.put("medve", "bear.png");
        text.put("blissjelkép", "blissymbol.png");
        text.put("kazetta", "cassette.png");
        text.put("almabor", "cider.png");
        text.put("kutya", "dog.png");
        text.put("érzés", "feeling.png");
        text.put("mappa", "folder.png");
        text.put("galaxis", "galaxy.png");
        text.put("borospohár", "goblet.png");
        text.put("remény", "hope.png");
        text.put("hosszú", "long.png");
        text.put("idő", "time.png");
        text.put("háború", "war.png");
        text.put("mentőautó","ambulance.png");
        text.put("űrhajós","astronaut.png");
        text.put("hőlégballon","balloon.png");
        text.put("batman","Batman.png");
        text.put("sör","beer.png");
        text.put("madár","bird.png");
        text.put("könyv","book.png");
        text.put("répa","carrot.png");
        text.put("cd","CD.png");
        text.put("cseresznye","cherry.png");
        text.put("elektromosság","electricity.png");
        text.put("zászló","flag.png");
        text.put("kalapács","hammer.png");
        text.put("fagylalt","ice_cream.png");
        text.put("sziget","island.png");
        text.put("szám","number.png");
        text.put("pingvin","penguin.png");
        text.put("eső","rain.png");

        return text;
    }
    //get random text from "database"
    public static String getRandomText(Hashtable<String, String> szavak) {
        int len = szavak.size();
        Random rn = new Random();
        int randomIndex = rn.nextInt(len);
        String[] names = szavak.keySet().toArray(new String[len]);
        return names[randomIndex];
    }
    
    /*
    *   Azt vettem észre miközben ezen dolgoztam hogy azt itt megalkotott "setLabelIcon()" -al buttonon is be lehet állítani a képeket
    *   gondolom mert alá van rendelve de lehet hogy ez csak bug aki tudja javítson ki! egyenlőre én még a button-hoz a buttonosat használtam
    */
    //setting up image for Buttons/ScaleImageButtons
    public static void setButtonIcon(String imagepath,Button bnt){
        Image img;
        try {
            img = Image.createImage(imagepath);
            bnt.setIcon(img);

        } catch (Exception e) {
                e.printStackTrace();
        }
    }
    //setting up image for Labels/ScaleImageLabels
    public static void setLabelIcon(String imagepath,Label lbl){
        Image img;
        try {
            img = Image.createImage(imagepath);
            lbl.setIcon(img);

        } catch (Exception e) {
                e.printStackTrace();
        }
    }
        // buttons array -> enable buttons
    public static void enableButtons(Button[] buttons){
        for (Button button : buttons) {                                                               
            button.setEnabled(true);
        }
    }
    // buttons array -> disable buttons
    public static void disableButtons(Button[] buttons){
        for (Button button : buttons) {
            button.setEnabled(false);
        }
    }
    // buttons array -> add button style
    public static void styleButtons(Button[] buttons){
        for (Button button : buttons) {
            button.getAllStyles().setBgTransparency(0xff);
            button.getAllStyles().setBgColor(0xffffff);
        }
    }
    public static void getSolution(List<word> words){
        words.clear();
        for(int i = 0; i < 4; i++){
            word data = new word();
            String reply = Config.getRandomText(Config.getHangmanData());
            for (word word : words) {
                while(word.getReply().contains(reply)){
                    reply = Config.getRandomText(Config.getHangmanData());
                }
            }
            String imagepath = "/" + Config.getHangmanData().get(reply);
            data.setImagePath(imagepath);
            data.setReply(reply);
            words.add(data);
        }
        Random rm = new Random();
        int truth = rm.nextInt(4);
        words.get(truth).setTruth(true);
    }

}
