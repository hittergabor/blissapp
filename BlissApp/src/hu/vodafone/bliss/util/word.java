/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.vodafone.bliss.util;

/**
 *
 * @author RYZEN
 */
public class word {
    private String reply;
    private String imagepath;
    private boolean truth;
    public void setTruth(boolean truth){
        this.truth = truth;
    }
    public boolean getTruth() {
        return truth;
    }
    public String getReply(){
        return reply;
    }
    public String getImagePath(){
        return imagepath;
    }
    public void setReply(String reply){
        this.reply = reply;
    }
    public void setImagePath(String imagepath){
        this.imagepath = imagepath;
    }
}
